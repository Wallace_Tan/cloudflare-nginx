#!/bin/sh
# vim:sw=4:ts=4:et

set -e

ME=$(basename $0)
DEFAULT_CONF_FILE="/etc/nginx/conf.d/default.conf"

if [ ! -f "$DEFAULT_CONF_FILE" ]; then
    echo >&3 "$ME: info: $DEFAULT_CONF_FILE is not a file or does not exist"
    exit 0
fi

# check if the file can be modified, e.g. not on a r/o filesystem
touch $DEFAULT_CONF_FILE 2>/dev/null || { echo >&3 "$ME: info: can not modify $DEFAULT_CONF_FILE (read-only file system?)"; exit 0; }

if [ ! -z "$APP_HOST" ]; then
    sed -i -E "s/[^[:blank:]]+:[0-9]+/${APP_HOST}:9000/g" $DEFAULT_CONF_FILE
    echo >&3 "$ME: info: Updated [${APP_HOST}:9000] in $DEFAULT_CONF_FILE"
else
    echo >&3 "$ME: info: Not updated: $DEFAULT_CONF_FILE"
fi

# LISTEN_PORT=3010
#echo "    listen       80;" |sed -E "s/(listen\s+)[0-9]+/\1 8080/"
if [ ! -z "$LISTEN_PORT" ]; then
    sed -i -E "s/(listen\s+)[0-9]+/\1${LISTEN_PORT}/g" $DEFAULT_CONF_FILE
    echo >&3 "$ME: info: Updated [listen ${LISTEN_PORT}] in $DEFAULT_CONF_FILE"
else
    echo >&3 "$ME: info: Not updated: $DEFAULT_CONF_FILE"
fi

exit 0
# docker compose exec nginx sh

# cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.BAK
# cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.TEST

# DEFAULT_CONF_FILE="/etc/nginx/conf.d/default.conf.TEST"
# LISTEN_PORT=3010

# head -n5 $DEFAULT_CONF_FILE
# sed -i.bak -E "s/(listen\s+)[0-9]+/\1${LISTEN_PORT}/g" $DEFAULT_CONF_FILE
# sed -E "s/(listen\s+)([0-9]+)/\1 \2->${LISTEN_PORT}/g" $DEFAULT_CONF_FILE | head -n5
# ls -al /etc/nginx/conf.d/